# peertube-plugin-logo-distrilab

DistriLab logo on peertube

## Building and publishing a new version
 - change the version in package.json
 - `npm install`
 - `npm run build`
 - test on a local peertube instance
 - `npm publish`
